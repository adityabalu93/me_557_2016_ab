//
//  main_spotlight.cpp
//  HCI 557 Spotlight example
//
//  Created by Rafael Radkowski on 5/28/15.
//  Copyright (c) 2015 -. All rights reserved.
//

// stl include
#include <iostream>
#include <string>
#include <map>

// GLEW include
#include <GL/glew.h>

// GLM include files
#define GLM_FORCE_INLINE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>


// glfw includes
#include <GLFW/glfw3.h>


// include local files
#include "controls.h"
#include "HCI557Common.h"
#include "CoordSystem.h"
#include "Plane3D.h"
#include "GLSphere.h"
#include "GLObjectObj.h"
#include "Texture.h"
#include "Box3D.h"



using namespace std;


// The handle to the window object
GLFWwindow*         window;

// Define some of the global variables we're using for this sample
GLuint program;

/* A trackball to move and rotate the camera view */
extern Trackball trackball;
extern int rotx;
extern int roty;
extern int rotz;
// this is a helper variable to allow us to change the texture blend model
extern int g_change_texture_blend;

typedef struct _keyframe
{
	float               _t; // the time fraction
	glm::vec3           _p; // the position
	glm::quat           _q; // the orientation

	/*
	Constructor
	*/
	_keyframe(float t, glm::vec3 p, glm::quat q)
	{
		_t = t;
		_p = p;
		_q = q;
	}

	/*
	Default constructor
	*/
	_keyframe()
	{
		_t = -1.0;
		_p = glm::vec3(0.0, 0.0, 0.0);
		_q = glm::quat(0.0, 0.0, 0.0, 0.0);
	}

	// prints the data into a terminal
	void print(void)
	{
		cout << "t: " << _t << "\tp: " << _p.x << ", " << _p.y << ", " << _p.z << "\tq: " << _q.x << ", " << _q.y << ", " << _q.z << ", " << _q.w << endl;
	}

}Keyframe;

/*
Type for the keyframe animation
*/
typedef map<double, Keyframe> KeyframeAnimation;

// Variable to store the keyframes
KeyframeAnimation myKeyframes;


/*!
@brief returns the time fraction for a given time and animation duration
@param time - the current animation time, application runtime, etc. in seconds
@param duration - the duration of the animation in seconds
@return the time fraction in an interval between 0 and 1.
*/
float getTimeFraction(const float time, const float duration)
{
	// we cast to an int. this results in the number of
	float interval = floor(time / duration);

	// return the current interval time
	float current_interval = time - interval*duration;

	// return the fraction / position in our current timeline
	float fraction = current_interval / duration;

	return fraction;
}



/*!
@brief returns the two keyframes for a given time.
@param keyframes - a map with all keyframes of type KeyframeAnimation
@param time - the time fraction between 0 and 1.
@param k0, reference to the first keyframe
@param k2, reference to the second keyframe
@return the number of keyframes. 1 if the time is equal to a keyframe, otherwise 2.
*/
int getKeyframes(KeyframeAnimation& keyframes, const double time, Keyframe& k0, Keyframe& k1)
{
	int num_keyframes = 0;

	// get a keyframe iterator
	KeyframeAnimation::iterator k_itr = keyframes.lower_bound(time);

	Keyframe k0_temp, k1_temp;

	// Obtain the first keyframe
	k1 = (*k_itr).second; num_keyframes++;


	// Check whether we are not at the beginning of this map
	if (k_itr != keyframes.begin())
	{
		k_itr--;  // decrement
		k0 = (*k_itr).second; // obtain the second keyframe
		num_keyframes++;
	}

	// write the first keyframe into k0 if we only have one
	if (num_keyframes == 1)
	{
		k0 = k1;
	}

	return num_keyframes;

}

/*!
@brief Interpolate between two keyframes
@param fraction - the time fraction for the interpolation / the location between two keyframes.
The value must be between 0 and 1.
@param k0, the start keyframe
@param k1, the end keyframe,
@param res, reference to a variable for the result.
*/
bool interpolateKeyframe(const float fraction, const Keyframe& k0, const Keyframe& k1, Keyframe& res)
{
	/////////////////////////////////////////////////////////////////////////
	// 1. Check the time delta

	// delta time
	float delta_t = k1._t - k0._t;

	// Check whether we have a delta time. Otherwise, we are at the location of exactly one keyframe
	if (delta_t == 0.0f){
		res = k0;
		return true;
	}

	/////////////////////////////////////////////////////////////////////////
	// 2. Interpolat the position

	// get the delta
	glm::vec3 delta_p = k1._p - k0._p;

	// position interpolation
	glm::vec3 p_int = k0._p + delta_p * (fraction - k0._t) / (delta_t);


	/////////////////////////////////////////////////////////////////////////
	// 3. Rotation interpolation

	// Calculate the distance between the target angle and the current angle.
	float delta_angle = sqrt((k1._q.x - k0._q.x)*(k1._q.x - k0._q.x) +
		(k1._q.y - k0._q.y)*(k1._q.y - k0._q.y) +
		(k1._q.z - k0._q.z)*(k1._q.z - k0._q.z) +
		(k1._q.w - k0._q.w)*(k1._q.w - k0._q.w));


	// Linear interpolation of the rotation using slerp
	glm::quat r_int = glm::slerp(k0._q, k1._q, (fraction - k0._t) / (delta_t));


	/////////////////////////////////////////////////////////////////////////
	// 4. Write the result
	res = Keyframe(fraction, p_int, r_int);

	return true;
}

/*!
This initializes the keyframes.
*/
void initKeyframeAnimation(void)
{
	myKeyframes[0.0] = Keyframe(0.0, glm::vec3(0.0, -10.0, -10.0), angleAxis(0.0f, glm::vec3(0.0, 0.0, 1.0)));
	myKeyframes[0.1] = Keyframe(0.1, glm::vec3(0.0, -9.0, -8.0), angleAxis(0.57f, glm::vec3(0.0, 0.0, 1.0)));
	myKeyframes[0.2] = Keyframe(0.2, glm::vec3(0.0, -8.0, -6.0), angleAxis(1.28f, glm::vec3(0.0, 0.0, 1.0)));
	myKeyframes[0.3] = Keyframe(0.3, glm::vec3(0.0, -7.0, -4.0), angleAxis(1.53f, glm::vec3(0.0, 0.0, 1.0)));
	myKeyframes[0.4] = Keyframe(0.4, glm::vec3(0.0, -6.0, -2.0), angleAxis(1.98f, glm::vec3(0.0, 0.1, 1.0)));
	myKeyframes[0.5] = Keyframe(0.5, glm::vec3(0.0, -5.0, 0.0), angleAxis(0.0f, glm::vec3(0.0, 0.0, 1.0)));
	myKeyframes[0.6] = Keyframe(0.6, glm::vec3(0.0, -4.0, 4.0), angleAxis(0.57f, glm::vec3(0.0, 0.0, 1.0)));
	myKeyframes[0.7] = Keyframe(0.7, glm::vec3(0.0, -2.5, 6.0), angleAxis(1.28f, glm::vec3(0.0, 0.0, 1.0)));
	myKeyframes[0.8] = Keyframe(0.8, glm::vec3(0.0, -1.0, 7.0), angleAxis(1.53f, glm::vec3(0.0, 0.0, 1.0)));
	myKeyframes[0.9] = Keyframe(0.9, glm::vec3(0.0, 1.0, 8.0), angleAxis(1.98f, glm::vec3(0.0, 0.1, 1.0)));
	myKeyframes[1.0] = Keyframe(1.0, glm::vec3(0.0, 5.0, 9.0), angleAxis(1.98f, glm::vec3(0.0, 0.1, 1.0)));



}


int main(int argc, const char * argv[])
{
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //// Init glfw, create a window, and init glew
    
    // Init the GLFW Window
    window = initWindow();
    
    
    // Init the glew api
    initGlew();
    
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //// Create some models
    
    // coordinate system
    CoordSystem* cs = new CoordSystem(40.0);
    
    
    // create an apperance object.
    GLAppearance* apperance_0 = new GLAppearance("shaders/multi_texture.vs", "shaders/multi_texture.fs");
	GLAppearance* apperance_1 = new GLAppearance("shaders/multi_texture.vs", "shaders/multi_texture.fs");
	GLAppearance* apperance_2 = new GLAppearance("shaders/multi_texture.vs", "shaders/multi_texture.fs");

    GLDirectLightSource  light_source;
    light_source._lightPos = glm::vec4(00.0,20.0,20.0, 0.0);
    light_source._ambient_intensity = 0.2;
    light_source._specular_intensity = 4.5;
    light_source._diffuse_intensity = 1.0;
    light_source._attenuation_coeff = 0.0;
    
    // add the light to this apperance object
    apperance_0->addLightSource(light_source);
	apperance_1->addLightSource(light_source);

    
    GLSpotLightSource spotlight_source;
    spotlight_source._lightPos = glm::vec4(0.0,00.0,50.0, 1.0);
    spotlight_source._ambient_intensity = 0.2;
    spotlight_source._specular_intensity = 30.5;
    spotlight_source._diffuse_intensity = 8.0;
    spotlight_source._attenuation_coeff = 0.0002;
    spotlight_source._cone_direction = glm::vec3(-1.0, -1.0,-1.0);
    spotlight_source._cone_angle = 20.0;
    
    apperance_0->addLightSource(spotlight_source);
	apperance_2->addLightSource(spotlight_source);

    // Create a material object
    GLMaterial material_0;
    material_0._diffuse_material = glm::vec3(0.1, 0.1, 0.1);
    material_0._ambient_material = glm::vec3(0.1, 0.1, 0.1);
    material_0._specular_material = glm::vec3(0.1, 0.1, 0.1);
    material_0._shininess = 12.0;
    material_0._transparency = 1.0;
    
	GLMaterial material_1;
	material_1._diffuse_material = glm::vec3(0.552, 0.333, 0.141);
	material_1._ambient_material = glm::vec3(0.552, 0.333, 0.141);
	material_1._specular_material = glm::vec3(0.552, 0.333, 0.141);
	material_1._shininess = 12.0;
	material_1._transparency = 0.5;

    // Add the material to the apperance object
    apperance_0->setMaterial(material_0);
	apperance_1->setMaterial(material_0);
	apperance_1->addLightSource(spotlight_source);
	apperance_2->setMaterial(material_1);

    
    //************************************************************************************************
    // Add a texture
    GLMultiTexture* texture = new GLMultiTexture();
	GLTexture* texture1 = new GLTexture();
	GLTexture* texture2 = NULL;
	int texid1 = texture1->loadAndCreateTexture("Cubemap.bmp");
	int texid = texture->loadAndCreateTextures("worldmap.bmp", "earthbump1k.bmp", "earthcloudmap.bmp");
    apperance_0->setTexture(texture);
	apperance_1->setTexture(texture1);
	apperance_2->setTexture(texture2);
    //************************************************************************************************
    // Finalize the appearance object
    apperance_0->finalize();
	apperance_1->finalize();
	apperance_2->finalize();

    
    // create the sphere geometry
	GLObjectObj* earth = new GLObjectObj("sphere.obj");
	GLObjectObj* human = new GLObjectObj("eagle.obj");
	GLBox3D* plane_0 = new GLBox3D(20, 20, 20,20,20);
	earth->setApperance(*apperance_0);
    earth->init();
	human->updatetextures();
	human->setApperance(*apperance_2);
	human->init();
	plane_0->setApperance(*apperance_1);
	plane_0->init();
	glm::mat4 tranform1 = glm::rotate(0.15f, glm::vec3(1.0f, 0.0f, 0.0f));
	glm::mat4 tranform2 = glm::rotate(0.32f, glm::vec3(0.0f, 1.0f, 0.0f));
	glm::mat4 tranform3 = glm::rotate(0.78f, glm::vec3(0.0f, 0.0f, 1.0f));
	glm::mat4 transform_multi = tranform3*tranform2 * tranform1;
	earth->setMatrix(transform_multi);
	glm::mat4 transform4 = glm::rotate(1.57f, glm::vec3(1.0f, 0.0f, 0.0f));
	human->setMatrix(transform4);
	// If you want to change appearance parameters after you init the object, call the update function
    apperance_0->updateLightSources();
	apperance_1->updateLightSources();


    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //// Main render loop
    
    // Set up our green background color
    static const GLfloat clear_color1[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	static const GLfloat clear_color2[] = { 0.529f, 0.808f, 0.98f, 1.0f };
    static const GLfloat clear_depth[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    
    // This sets the camera to a new location
    // the first parameter is the eye position, the second the center location, and the third the up vector.
	initKeyframeAnimation();

    
    // Enable depth test
    // ignore this line, it allows us to keep the distance value after we proejct each object to a 2d canvas.
    glEnable(GL_DEPTH_TEST);
    
	SetCameraManipulator(CameraTypes::CAMERA_MANIPULATOR);

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //// Blending
    
    // Enable blending
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    

    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //// Main render loop
    
    // This is our render loop. As long as our window remains open (ESC is not pressed), we'll continue to render things.
    while(!glfwWindowShouldClose(window))
    {
		//glClearBufferfv(GL_COLOR, 0, clear_color1);
        // Clear the entire buffer with our green color (sets the background to be green).
        glClearBufferfv(GL_DEPTH , 0, clear_depth);
        

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //// This renders the objects
        
        // Set the trackball locatiom
		SetTrackballLocation(GetCurrentCameraMatrix(), GetCurrentCameraTranslation());
		// draw the objects
		//cs->draw();

		if (GetCameraDistance() > 11.0){
			glClearBufferfv(GL_COLOR, 0, clear_color1);
			//SetTrackballLocation(GetCurrentCameraMatrix(), GetCurrentCameraTranslation());
			earth->draw();

		}
		else {
			glClearBufferfv(GL_COLOR, 0, clear_color2);
			plane_0->draw();

			// Interpolate between keyframes
			Keyframe k0, k1, k_res;

			float time = glfwGetTime();

			float f = getTimeFraction(time, 8.0); // we assume that the animation takes 8 seconds

			int num = getKeyframes(myKeyframes, f, k0, k1);

			bool ret = interpolateKeyframe(f, k0, k1, k_res);

			glm::vec3 t = k_res._p;
			glm::mat4 tmat = glm::translate(t);
			glm::mat4 rot_init = glm::rotate(1.57f, glm::vec3(0.0f, 1.0f, 0.0f));
			
			human->setMatrix(transform4*rot_init*tmat);
			human->draw();
		}
        // change the texture appearance blend mode
        bool ret = texture->setTextureBlendMode(g_change_texture_blend);
        if(ret)apperance_0->updateTextures();
		apperance_1->updateTextures();
		//earth->Rot_transform1(rotx);
		//earth->Rot_transform2(roty);
		//earth->Rot_transform3(rotz);
        
        //// This renders the objects
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        
        // Swap the buffers so that what we drew will appear on the screen.
        glfwSwapBuffers(window);
        glfwPollEvents();
        
    }
    
    
    delete cs;
    
    
}

