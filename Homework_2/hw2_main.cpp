//
//  main.cpp
//  OpenGL4Test
//
//  Created by Rafael Radkowski on 5/28/15.
//  Copyright (c) 2015 -. All rights reserved.
//

// stl include
#include <iostream>
#include <string>

// GLEW include
#include <GL/glew.h>

// GLM include files
#define GLM_FORCE_INLINE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>


// glfw includes
#include <GLFW/glfw3.h>


// include local files
#include "controls.h"
#include "HCI557Common.h"
#include "CoordSystem.h"

// this line tells the compiler to use the namespace std.
// Each object, command without a namespace is assumed to be part of std. 
using namespace std;




static const string vs_string =
"#version 410 core                                                 \n"
"                                                                   \n"
"uniform mat4 projectionMatrix;                                    \n"
"uniform mat4 viewMatrix;                                           \n"
"uniform mat4 modelMatrix;                                          \n"
"in vec3 in_Position;                                               \n"
"                                                                   \n"
"in vec3 in_Color;                                                  \n"
"out vec3 pass_Color;                                               \n"
"                                                                  \n"
"void main(void)                                                   \n"
"{                                                                 \n"
"    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(in_Position, 1.0);  \n"
"    pass_Color = in_Color;                                         \n"
"}                                                                 \n";

// Fragment shader source code. This determines the colors in the fragment generated in the shader pipeline. In this case, it colors the inside of our triangle specified by our vertex shader.
static const string fs_string  =
"#version 410 core                                                 \n"
"                                                                  \n"
"in vec3 pass_Color;                                                 \n"
"out vec4 color;                                                    \n"
"void main(void)                                                   \n"
"{                                                                 \n"
"    color = vec4(pass_Color, 1.0);                               \n"
"}                                                                 \n";




/// Camera control matrices
glm::mat4 projectionMatrix; // Store the projection matrix
glm::mat4 viewMatrix; // Store the view matrix
glm::mat4 modelMatrix; // Store the model matrix




// The handle to the window object
GLFWwindow*         window;


// Define some of the global variables we're using for this sample
GLuint program;







///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Fill this functions with your model code.

// USE THESE vertex array objects to define your objects
unsigned int vaoID[2];

unsigned int vboID[4];

/*!
 ADD YOUR CODE TO CREATE THE TRIANGLE STRIP MODEL TO THIS FUNCTION
 */
unsigned int createTriangleStripModel(void)
{
	float *vertices = new float[147];
	float *colors = new float[147];
	float r, g, b;
	r = 0.7;
	g = 0.3;
	b = 0.0;

	//FRONT FACE
	vertices[0] = 0.0; vertices[1] = 0.0; vertices[2] = 0.0;
	colors[0] = r; colors[1] = g; colors[2] = b;
	vertices[3] = 0.0; vertices[4] = 0.0; vertices[5] = 1.0;
	colors[3] = r; colors[4] = g; colors[5] = b;
	vertices[6] = 1.0; vertices[7] = 0.0; vertices[8] = 0.0;
	colors[6] = r; colors[7] = g; colors[8] = b;
	vertices[9] = 1.0; vertices[10] = 0.0; vertices[11] = 1.0;
	colors[9] = r; colors[10] = g; colors[11] = b;

	// right side face
	vertices[12] = 1.0; vertices[13] = 2.0; vertices[14] = 0.0;
	colors[12] = r; colors[13] = g; colors[14] = b;
	vertices[15] = 1.0; vertices[16] = 2.0; vertices[17] = 1.0;
	colors[15] = r; colors[16] = g; colors[17] = b;

	//right sided front face
	vertices[18] = 3.0; vertices[19] = 2.0; vertices[20] = 0.0;
	colors[18] = r; colors[19] = g; colors[20] = b;
	vertices[21] = 3.0; vertices[22] = 2.0; vertices[23] = 1.0;
	colors[21] = r; colors[22] = g; colors[23] = b;

	//right sided right face
	vertices[24] = 3.0; vertices[25] = 3.0; vertices[26] = 0.0;
	colors[24] = r; colors[25] = g; colors[26] = b;
	vertices[27] = 3.0; vertices[28] = 3.0; vertices[29] = 1.0;
	colors[21] = r; colors[22] = g; colors[23] = b;

	//back side bottom face
	vertices[30] = 0.0; vertices[31] = 3.0; vertices[32] = 0.0;
	colors[30] = r; colors[31] = g; colors[32] = b;
	vertices[33] = 0.0; vertices[34] = 3.0; vertices[35] = 1.0;
	colors[33] = r; colors[34] = g; colors[35] = b;

	vertices[36] = 0.0; vertices[37] = 0.0; vertices[38] = 0.0;
	colors[36] = r; colors[37] = g; colors[38] = b;

	vertices[39] = 0.0; vertices[40] = 0.0; vertices[41] = 1.0;
	colors[39] = r; colors[40] = g; colors[41] = b;

	vertices[42] = 0.0; vertices[43] = 0.0; vertices[44] = 1.0;
	colors[42] = r; colors[43] = g; colors[44] = b;

	vertices[45] = 1.0; vertices[46] = 0.0; vertices[47] = 1.0;
	colors[45] = r; colors[46] = g; colors[47] = b;

	vertices[48] = 0.0; vertices[49] = 2.0; vertices[50] = 1.0;
	colors[48] = r; colors[49] = g; colors[50] = b;

	vertices[51] = 1.0; vertices[52] = 2.0; vertices[53] = 1.0;
	colors[51] = r; colors[52] = g; colors[53] = b;

	vertices[54] = 0.0; vertices[55] = 2.0; vertices[56] = 2.0;
	colors[54] = r; colors[55] = g; colors[56] = b;

	vertices[57] = 1.0; vertices[58] = 2.0; vertices[59] = 2.0;
	colors[57] = r; colors[58] = g; colors[59] = b;

	vertices[60] = 0.0; vertices[61] = 3.0; vertices[62] = 2.0;
	colors[60] = r; colors[61] = g; colors[62] = b;

	vertices[63] = 1.0; vertices[64] = 3.0; vertices[65] = 2.0;
	colors[63] = r; colors[64] = g; colors[65] = b;

	vertices[66] = 1.0; vertices[67] = 3.0; vertices[68] = 2.0;
	colors[66] = r; colors[67] = g; colors[68] = b;

	vertices[66] = 1.0; vertices[67] = 2.0; vertices[68] = 2.0;
	colors[66] = r; colors[67] = g; colors[68] = b;

	vertices[69] = 3.0; vertices[70] = 3.0; vertices[71] = 1.0;
	colors[69] = r; colors[70] = g; colors[71] = b;

	vertices[72] = 3.0; vertices[73] = 2.0; vertices[74] = 1.0;
	colors[72] = r; colors[73] = g; colors[74] = b;

	vertices[75] = 3.0; vertices[76] = 2.0; vertices[77] = 1.0;
	colors[75] = r; colors[76] = g; colors[77] = b;

	vertices[78] = 3.0; vertices[79] = 2.0; vertices[80] = 0.0;
	colors[78] = r; colors[79] = g; colors[80] = b;

	vertices[81] = 3.0; vertices[82] = 2.0; vertices[83] = 0.0;
	colors[81] = r; colors[82] = g; colors[83] = b;

	vertices[84] = 3.0; vertices[85] = 3.0; vertices[86] = 0.0;
	colors[84] = r; colors[85] = g; colors[86] = b;

	vertices[87] = 1.0; vertices[88] = 2.0; vertices[89] = 0.0;
	colors[87] = r; colors[88] = g; colors[89] = b;


	vertices[90] = 1.0; vertices[91] = 3.0; vertices[92] = 0.0;
	colors[90] = r; colors[91] = g; colors[92] = b;

	vertices[93] = 0.0; vertices[94] = 2.0; vertices[95] = 0.0;
	colors[93] = r; colors[94] = g; colors[95] = b;

	vertices[96] = 0.0; vertices[97] = 3.0; vertices[98] = 0.0;
	colors[96] = r; colors[97] = g; colors[98] = b;


	vertices[99] = 0.0; vertices[100] = 2.0; vertices[101] = 0.0;
	colors[99] = r; colors[100] = g; colors[101] = b;


	vertices[102] = 0.0; vertices[103] = 0.0; vertices[104] = 0.0;
	colors[102] = r; colors[103] = g; colors[104] = b;


	vertices[105] = 1.0; vertices[106] = 2.0; vertices[107] = 0.0;
	colors[105] = r; colors[106] = g; colors[107] = b;


	vertices[108] = 1.0; vertices[109] = 0.0; vertices[110] = 0.0;
	colors[108] = r; colors[109] = g; colors[110] = b;
	
	/// patching the rest of the areas

	vertices[111] = 0.0; vertices[112] = 2.0; vertices[113] = 1.0;
	colors[111] = r; colors[112] = g; colors[113] = b;
	
	vertices[114] = 0.0; vertices[115] = 2.0; vertices[116] = 2.0;
	colors[114] = r; colors[115] = g; colors[116] = b;
	vertices[117] = 0.0; vertices[118] = 3.0; vertices[119] = 1.0;
	colors[117] = r; colors[118] = g; colors[119] = b;
	vertices[120] = 0.0; vertices[121] = 3.0; vertices[122] = 2.0;
	colors[120] = r; colors[121] = g; colors[122] = b;
	
	vertices[123] = 1.0; vertices[124] = 3.0; vertices[125] = 1.0;
	colors[123] = r; colors[124] = g; colors[125] = b;
	
	vertices[126] = 1.0; vertices[127] = 3.0; vertices[128] = 2.0;
	colors[126] = r; colors[127] = g; colors[128] = b;

	vertices[129] = 3.0; vertices[130] = 3.0; vertices[131] = 1.0;
	colors[129] = r; colors[130] = g; colors[131] = b;

	vertices[132] = 3.0; vertices[133] = 3.0; vertices[134] = 1.0;
	colors[132] = r; colors[133] = g; colors[134] = b;

	vertices[135] = 3.0; vertices[136] = 2.0; vertices[137] = 1.0;
	colors[135] = r; colors[136] = g; colors[137] = b;

	vertices[138] = 3.0; vertices[139] = 2.0; vertices[140] = 1.0;
	colors[138] = r; colors[139] = g; colors[140] = b;

	vertices[141] = 1.0; vertices[142] = 2.0; vertices[143] = 1.0;
	colors[141] = r; colors[142] = g; colors[143] = b;
	
	vertices[144] = 1.0; vertices[145] = 2.0; vertices[146] = 2.0;
	colors[144] = r; colors[145] = g; colors[146] = b;
	
	
	glGenVertexArrays(2, &vaoID[0]);
	glBindVertexArray(vaoID[0]);

	glGenBuffers(2, vboID);

	// vertices
	glBindBuffer(GL_ARRAY_BUFFER, vboID[0]);
	glBufferData(GL_ARRAY_BUFFER, 147 * sizeof(GLfloat), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);


	// color
	glBindBuffer(GL_ARRAY_BUFFER, vboID[1]);
	glBufferData(GL_ARRAY_BUFFER, 147 * sizeof(GLfloat), colors, GL_STATIC_DRAW);

	glVertexAttribPointer((GLuint)1, 3, GL_FLOAT, GL_FALSE, 0, 0); // Set up our vertex attributes pointer
	glEnableVertexAttribArray(1);
	glBindVertexArray(0);

	delete[] vertices;
	return 1;
}

/*!
 ADD YOUR CODE TO CREATE A MODEL USING PRIMITIVES OF YOUR CHOISE TO THIS FUNCTION
 */
unsigned int createTriangleModel(void)
{
	// use the vertex array object vaoID[1] for this model representation

	float *vertices = new float[270];
	float *colors = new float[270];

	//FRONT FACE
	vertices[0] = 0.0; vertices[1] = 0.0; vertices[2] = 0.0;
	colors[0] = 0.0; colors[1] = 0.0; colors[2] = 1.0;
	vertices[3] = 0.0; vertices[4] = 0.0; vertices[5] = 1.0;
	colors[3] = 0.0; colors[4] = 0.0; colors[5] = 1.0;
	vertices[6] = 1.0; vertices[7] = 0.0; vertices[8] = 0.0;
	colors[6] = 0.0; colors[7] = 0.0; colors[8] = 1.0;

	vertices[9] = 1.0; vertices[10] = 0.0; vertices[11] = 0.0;
	colors[9] = 0.0; colors[10] = 0.0; colors[11] = 1.0;
	vertices[12] = 1.0; vertices[13] = 0.0; vertices[14] = 1.0;
	colors[12] = 0.0; colors[13] = 0.0; colors[14] = 1.0;
	vertices[15] = 0.0; vertices[16] = 0.0; vertices[17] = 1.0;
	colors[15] = 0.0; colors[16] = 0.0; colors[17] = 1.0;

	//face 2
	vertices[18] = 1.0; vertices[19] = 0.0; vertices[20] = 0.0;
	colors[18] = 0.0; colors[19] = 0.0; colors[20] = 1.0;
	vertices[21] = 1.0; vertices[22] = 0.0; vertices[23] = 1.0;
	colors[21] = 0.0; colors[22] = 0.0; colors[23] = 1.0;
	vertices[24] = 1.0; vertices[25] = 2.0; vertices[26] = 0.0;
	colors[24] = 0.0; colors[25] = 0.0; colors[26] = 1.0;

	vertices[27] = 1.0; vertices[28] = 0.0; vertices[29] = 1.0;
	colors[27] = 0.0; colors[28] = 0.0; colors[29] = 1.0;
	vertices[30] = 1.0; vertices[31] = 2.0; vertices[32] = 0.0;
	colors[30] = 0.0; colors[31] = 0.0; colors[32] = 1.0;
	vertices[33] = 1.0; vertices[34] = 2.0; vertices[35] = 1.0;
	colors[33] = 0.0; colors[34] = 0.0; colors[35] = 1.0;

	//face 3
	vertices[36] = 1.0; vertices[37] = 2.0; vertices[38] = 0.0;
	colors[36] = 0.0; colors[37] = 0.0; colors[38] = 1.0;
	vertices[39] = 1.0; vertices[40] = 2.0; vertices[41] = 1.0;
	colors[39] = 0.0; colors[40] = 0.0; colors[41] = 1.0;
	vertices[42] = 3.0; vertices[43] = 2.0; vertices[44] = 0.0;
	colors[42] = 0.0; colors[43] = 0.0; colors[44] = 1.0;

	vertices[45] = 1.0; vertices[46] = 2.0; vertices[47] = 1.0;
	colors[45] = 0.0; colors[46] = 0.0; colors[47] = 1.0;
	vertices[48] = 3.0; vertices[49] = 2.0; vertices[50] = 1.0;
	colors[48] = 0.0; colors[49] = 0.0; colors[50] = 1.0;
	vertices[51] = 3.0; vertices[52] = 2.0; vertices[53] = 0.0;
	colors[51] = 0.0; colors[52] = 0.0; colors[53] = 1.0;

	//face 4
	vertices[54] = 3.0; vertices[55] = 2.0; vertices[56] = 0.0;
	colors[54] = 0.0; colors[55] = 0.0; colors[56] = 1.0;
	vertices[57] = 3.0; vertices[58] = 2.0; vertices[59] = 1.0;
	colors[57] = 0.0; colors[58] = 0.0; colors[59] = 1.0;
	vertices[60] = 3.0; vertices[61] = 3.0; vertices[62] = 0.0;
	colors[60] = 0.0; colors[61] = 0.0; colors[62] = 1.0;

	vertices[63] = 3.0; vertices[64] = 3.0; vertices[65] = 0.0;
	colors[63] = 0.0; colors[64] = 0.0; colors[65] = 1.0;
	vertices[66] = 3.0; vertices[67] = 3.0; vertices[68] = 1.0;
	colors[66] = 0.0; colors[67] = 0.0; colors[68] = 1.0;
	vertices[69] = 3.0; vertices[70] = 2.0; vertices[71] = 1.0;
	colors[69] = 0.0; colors[70] = 0.0; colors[71] = 1.0;

	//face 5
	vertices[72] = 3.0; vertices[73] = 3.0; vertices[74] = 0.0;
	colors[72] = 0.0; colors[73] = 0.0; colors[74] = 1.0;
	vertices[75] = 3.0; vertices[76] = 3.0; vertices[77] = 1.0;
	colors[75] = 0.0; colors[76] = 0.0; colors[77] = 1.0;
	vertices[78] = 0.0; vertices[79] = 3.0; vertices[80] = 0.0;
	colors[78] = 0.0; colors[79] = 0.0; colors[80] = 0.0;

	vertices[81] = 3.0; vertices[82] = 3.0; vertices[83] = 1.0;
	colors[81] = 0.0; colors[82] = 0.0; colors[83] = 1.0;
	vertices[84] = 0.0; vertices[85] = 3.0; vertices[86] = 1.0;
	colors[84] = 0.0; colors[85] = 0.0; colors[86] = 1.0;
	vertices[87] = 0.0; vertices[88] = 3.0; vertices[89] = 0.0;
	colors[87] = 0.0; colors[88] = 0.0; colors[89] = 1.0;

	//face 6
	vertices[90] = 0.0; vertices[91] = 3.0; vertices[92] = 0.0;
	colors[90] = 0.0; colors[91] = 0.0; colors[92] = 1.0;
	vertices[93] = 0.0; vertices[94] = 3.0; vertices[95] = 1.0;
	colors[93] = 0.0; colors[94] = 0.0; colors[95] = 1.0;
	vertices[96] = 0.0; vertices[97] = 0.0; vertices[98] = 0.0;
	colors[96] = 0.0; colors[97] = 0.0; colors[98] = 1.0;
	//////////////////////
	vertices[99] = 0.0; vertices[100] = 3.0; vertices[101] = 1.0;
	colors[99] = 0.0; colors[100] = 0.0; colors[101] = 1.0;
	vertices[102] = 0.0; vertices[103] = 0.0; vertices[104] = 0.0;
	colors[102] = 0.0; colors[103] = 0.0; colors[104] = 1.0;
	vertices[105] = 0.0; vertices[106] = 0.0; vertices[107] = 1.0;
	colors[105] = 0.0; colors[106] = 0.0; colors[107] = 1.0;

	//face 7
	vertices[108] = 0.0; vertices[109] = 0.0; vertices[110] = 1.0;
	colors[108] = 0.0; colors[109] = 0.0; colors[110] = 1.0;
	vertices[111] = 1.0; vertices[112] = 0.0; vertices[113] = 1.0;
	colors[111] = 0.0; colors[112] = 0.0; colors[113] = 1.0;
	vertices[114] = 1.0; vertices[115] = 2.0; vertices[116] = 1.0;
	colors[114] = 0.0; colors[115] = 0.0; colors[116] = 1.0;

	vertices[117] = 0.0; vertices[118] = 0.0; vertices[119] = 1.0;
	colors[117] = 0.0; colors[118] = 0.0; colors[119] = 1.0;
	vertices[120] = 0.0; vertices[121] = 2.0; vertices[122] = 1.0;
	colors[120] = 0.0; colors[121] = 0.0; colors[122] = 1.0;
	vertices[123] = 1.0; vertices[124] = 2.0; vertices[125] = 1.0;
	colors[123] = 0.0; colors[124] = 0.0; colors[125] = 1.0;
	///////////////////////
	//face 8
	vertices[126] = 0.0; vertices[127] = 2.0; vertices[128] = 1.0;
	colors[126] = 0.0; colors[127] = 0.0; colors[128] = 1.0;
	vertices[129] = 1.0; vertices[130] = 2.0; vertices[131] = 1.0;
	colors[129] = 0.0; colors[130] = 0.0; colors[131] = 1.0;
	vertices[132] = 1.0; vertices[133] = 2.0; vertices[134] = 2.0;
	colors[132] = 0.0; colors[133] = 0.0; colors[134] = 1.0;

	vertices[135] = 0.0; vertices[136] = 2.0; vertices[137] = 1.0;
	colors[135] = 0.0; colors[136] = 0.0; colors[137] = 1.0;
	vertices[138] = 0.0; vertices[139] = 2.0; vertices[140] = 2.0;
	colors[138] = 0.0; colors[139] = 0.0; colors[140] = 1.0;
	vertices[141] = 1.0; vertices[142] = 2.0; vertices[143] = 2.0;
	colors[141] = 0.0; colors[142] = 0.0; colors[143] = 1.0;

	//////////////////////
	//face 9
	vertices[144] = 1.0; vertices[145] = 2.0; vertices[146] = 1.0;
	colors[144] = 0.0; colors[145] = 0.0; colors[146] = 1.0;
	vertices[147] = 1.0; vertices[148] = 2.0; vertices[149] = 2.0;
	colors[147] = 0.0; colors[148] = 0.0; colors[149] = 1.0;
	vertices[150] = 3.0; vertices[151] = 2.0; vertices[152] = 1.0;
	colors[150] = 0.0; colors[151] = 0.0; colors[152] = 1.0;

	//face 10
	vertices[153] = 1.0; vertices[154] = 3.0; vertices[155] = 1.0;
	colors[153] = 0.0; colors[154] = 0.0; colors[155] = 1.0;
	vertices[156] = 3.0; vertices[157] = 3.0; vertices[158] = 1.0;
	colors[156] = 0.0; colors[157] = 0.0; colors[158] = 1.0;
	vertices[159] = 1.0; vertices[160] = 3.0; vertices[161] = 2.0;
	colors[159] = 0.0; colors[160] = 0.0; colors[161] = 1.0;

	//face 11
	vertices[162] = 0.0; vertices[163] = 2.0; vertices[164] = 2.0;
	colors[162] = 0.0; colors[163] = 0.0; colors[164] = 1.0;
	vertices[165] = 1.0; vertices[166] = 2.0; vertices[167] = 2.0;
	colors[165] = 0.0; colors[166] = 0.0; colors[167] = 1.0;
	vertices[168] = 1.0; vertices[169] = 3.0; vertices[170] = 2.0;
	colors[168] = 0.0; colors[169] = 0.0; colors[170] = 1.0;

	vertices[171] = 0.0; vertices[172] = 3.0; vertices[173] = 2.0;
	colors[171] = 0.0; colors[172] = 0.0; colors[173] = 1.0;
	vertices[174] = 1.0; vertices[175] = 3.0; vertices[176] = 2.0;
	colors[174] = 0.0; colors[175] = 0.0; colors[176] = 1.0;
	vertices[177] = 0.0; vertices[178] = 2.0; vertices[179] = 2.0;
	colors[177] = 0.0; colors[178] = 0.0; colors[179] = 1.0;

	//face 12
	vertices[180] = 0.0; vertices[181] = 2.0; vertices[182] = 1.0;
	colors[180] = 0.0; colors[181] = 0.0; colors[182] = 1.0;
	vertices[183] = 0.0; vertices[184] = 3.0; vertices[185] = 1.0;
	colors[183] = 0.0; colors[184] = 0.0; colors[185] = 1.0;
	vertices[186] = 0.0; vertices[187] = 3.0; vertices[188] = 2.0;
	colors[186] = 0.0; colors[187] = 0.0; colors[188] = 1.0;

	vertices[189] = 0.0; vertices[190] = 2.0; vertices[191] = 1.0;
	colors[189] = 0.0; colors[190] = 0.0; colors[191] = 1.0;
	vertices[192] = 0.0; vertices[193] = 2.0; vertices[194] = 2.0;
	colors[192] = 0.0; colors[193] = 0.0; colors[194] = 1.0;
	vertices[195] = 0.0; vertices[196] = 3.0; vertices[197] = 2.0;
	colors[195] = 0.0; colors[196] = 0.0; colors[197] = 1.0;

	///////////////////////////////////////////////
	//face 13
	vertices[198] = 0.0; vertices[199] = 3.0; vertices[200] = 1.0;
	colors[198] = 0.0; colors[199] = 0.0; colors[200] = 1.0;
	vertices[201] = 1.0; vertices[202] = 3.0; vertices[203] = 1.0;
	colors[201] = 0.0; colors[202] = 0.0; colors[203] = 1.0;
	vertices[204] = 1.0; vertices[205] = 3.0; vertices[206] = 2.0;
	colors[204] = 0.0; colors[205] = 0.0; colors[206] = 1.0;

	vertices[207] = 0.0; vertices[208] = 3.0; vertices[209] = 1.0;
	colors[207] = 0.0; colors[208] = 0.0; colors[209] = 1.0;
	vertices[210] = 1.0; vertices[211] = 3.0; vertices[212] = 2.0;
	colors[210] = 0.0; colors[211] = 0.0; colors[212] = 1.0;
	vertices[213] = 0.0; vertices[214] = 3.0; vertices[215] = 2.0;
	colors[213] = 0.0; colors[214] = 0.0; colors[215] = 1.0;

	//face 14
	vertices[216] = 1.0; vertices[217] = 2.0; vertices[218] = 2.0;
	colors[216] = 0.0; colors[217] = 0.0; colors[218] = 1.0;
	vertices[219] = 3.0; vertices[220] = 2.0; vertices[221] = 1.0;
	colors[219] = 0.0; colors[220] = 0.0; colors[221] = 1.0;
	vertices[222] = 3.0; vertices[223] = 3.0; vertices[224] = 1.0;
	colors[222] = 0.0; colors[223] = 0.0; colors[224] = 1.0;

	vertices[225] = 1.0; vertices[226] = 2.0; vertices[227] = 2.0;
	colors[225] = 0.0; colors[226] = 0.0; colors[227] = 1.0;
	vertices[228] = 1.0; vertices[229] = 3.0; vertices[230] = 2.0;
	colors[228] = 0.0; colors[229] = 0.0; colors[230] = 1.0;
	vertices[231] = 3.0; vertices[232] = 3.0; vertices[233] = 1.0;
	colors[231] = 0.0; colors[232] = 0.0; colors[233] = 1.0;

	//face 15
	vertices[234] = 0.0; vertices[235] = 0.0; vertices[236] = 0.0;
	colors[234] = 0.0; colors[235] = 0.0; colors[236] = 1.0;
	vertices[237] = 1.0; vertices[238] = 0.0; vertices[239] = 0.0;
	colors[237] = 0.0; colors[238] = 0.0; colors[239] = 1.0;
	vertices[240] = 1.0; vertices[241] = 3.0; vertices[242] = 0.0;
	colors[240] = 0.0; colors[241] = 0.0; colors[242] = 1.0;

	vertices[243] = 0.0; vertices[244] = 0.0; vertices[245] = 0.0;
	colors[243] = 0.0; colors[244] = 0.0; colors[245] = 1.0;
	vertices[246] = 1.0; vertices[247] = 3.0; vertices[248] = 0.0;
	colors[246] = 0.0; colors[247] = 0.0; colors[248] = 1.0;
	vertices[249] = 0.0; vertices[250] = 3.0; vertices[251] = 0.0;
	colors[249] = 0.0; colors[250] = 0.0; colors[251] = 1.0;
	
	//face 16
	vertices[252] = 1.0; vertices[253] = 2.0; vertices[254] = 0.0;
	colors[252] = 0.0; colors[253] = 0.0; colors[254] = 1.0;
	vertices[255] = 3.0; vertices[256] = 2.0; vertices[257] = 0.0;
	colors[255] = 0.0; colors[256] = 0.0; colors[257] = 1.0;
	vertices[258] = 3.0; vertices[259] = 3.0; vertices[260] = 0.0;
	colors[258] = 0.0; colors[259] = 0.0; colors[260] = 1.0;

	vertices[261] = 1.0; vertices[262] = 2.0; vertices[263] = 0.0;
	colors[261] = 0.0; colors[262] = 0.0; colors[263] = 1.0;
	vertices[264] = 1.0; vertices[265] = 3.0; vertices[266] = 0.0;
	colors[264] = 0.0; colors[265] = 0.0; colors[266] = 1.0;
	vertices[267] = 3.0; vertices[268] = 3.0; vertices[269] = 0.0;
	colors[267] = 0.0; colors[268] = 0.0; colors[269] = 1.0;
	

	glGenVertexArrays(2, &vaoID[1]);
	glBindVertexArray(vaoID[1]);

	glGenBuffers(2, vboID);

	// vertices
	glBindBuffer(GL_ARRAY_BUFFER, vboID[0]);
	glBufferData(GL_ARRAY_BUFFER, 270 * sizeof(GLfloat), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);


	// color
	glBindBuffer(GL_ARRAY_BUFFER, vboID[1]);
	glBufferData(GL_ARRAY_BUFFER, 270 * sizeof(GLfloat), colors, GL_STATIC_DRAW);

	glVertexAttribPointer((GLuint)1, 3, GL_FLOAT, GL_FALSE, 0, 0); // Set up our vertex attributes pointer
	glEnableVertexAttribArray(1);
	glBindVertexArray(0);

	delete[] vertices;
	return 1;
}



/*!
 ADD YOUR CODE TO RENDER THE TRIANGLE STRIP MODEL TO THIS FUNCTION
 */
void renderTriangleStripModel(void)
{

    // Bind the buffer and switch it to an active buffer
    glBindVertexArray(vaoID[0]);
        
	// HERE: THIS CAUSES AN ERROR BECAUSE I DO NOT KNOW HOW MANY TRIANGLES / VERTICES YOU HAVE.
	// COMPLETE THE LINE
    // Draw the triangles
    glDrawArrays(GL_TRIANGLE_STRIP, 0 , 14 );
	glDrawArrays(GL_TRIANGLE_STRIP, 13, 22);
	glDrawArrays(GL_TRIANGLE_STRIP, 25, 24);
    // Unbind our Vertex Array Object
    glBindVertexArray(0);
}



/*!
 ADD YOUR CODE TO RENDER THE TRIANGLE STRIP MODEL TO THIS FUNCTION
 */
void renderTriangleModel(void)
{

    // Bind the buffer and switch it to an active buffer
    glBindVertexArray(vaoID[1]);
        

	// HERE: THIS CAUSES AN ERROR BECAUSE I DO NOT KNOW HOW MANY POLYGONS YOU HAVE.
	// COMPLETE THE LINE
    // Draw the triangles
    glDrawArrays(GL_TRIANGLES, 0 , 270);

    // Unbind our Vertex Array Object
    glBindVertexArray(0);
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/*!
 This function creates the two models
 */
void setupScene(void) {
    
    createTriangleStripModel();
	createTriangleModel();
    
}




int main(int argc, const char * argv[])
{
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //// Init glfw, create a window, and init glew
    
    // Init the GLFW Window
    window = initWindow();
    
    
    // Init the glew api
    initGlew();
    
	// Prepares some defaults
	CoordSystemRenderer* coordinate_system_renderer = new CoordSystemRenderer(10.0);

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //// The Shader Program starts here
    
    // Vertex shader source code. This draws the vertices in our window. We have 3 vertices since we're drawing an triangle.
    // Each vertex is represented by a vector of size 4 (x, y, z, w) coordinates.
    static const string vertex_code = vs_string;
    static const char * vs_source = vertex_code.c_str();
    
    // Fragment shader source code. This determines the colors in the fragment generated in the shader pipeline. In this case, it colors the inside of our triangle specified by our vertex shader.
    static const string fragment_code = fs_string;
    static const char * fs_source = fragment_code.c_str();
    
    // This next section we'll generate the OpenGL program and attach the shaders to it so that we can render our triangle.
    program = glCreateProgram();
    
    // We create a shader with our fragment shader source code and compile it.
    GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fs, 1, &fs_source, NULL);
    glCompileShader(fs);
    
    // We create a shader with our vertex shader source code and compile it.
    GLuint vs = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vs, 1, &vs_source, NULL);
    glCompileShader(vs);
    
    // We'll attach our two compiled shaders to the OpenGL program.
    glAttachShader(program, vs);
    glAttachShader(program, fs);
    
    glLinkProgram(program);
    
    // We'll specify that we want to use this program that we've attached the shaders to.
    glUseProgram(program);
    
    //// The Shader Program ends here
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    /// IGNORE THE NEXT PART OF THIS CODE
    /// IGNORE THE NEXT PART OF THIS CODE
    /// IGNORE THE NEXT PART OF THIS CODE
    // It controls the virtual camera
    
    // Set up our green background color
    static const GLfloat clear_color[] = { 0.6f, 0.7f, 1.0f, 1.0f };
    static const GLfloat clear_depth[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    
    
    projectionMatrix = glm::perspective(1.1f, (float)800 / (float)600, 0.1f, 100.f);
    modelMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)); // Create our model matrix which will halve the size of our model
    viewMatrix = glm::lookAt(glm::vec3(1.0f, 0.0f, 5.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    
    int projectionMatrixLocation = glGetUniformLocation(program, "projectionMatrix"); // Get the location of our projection matrix in the shader
    int viewMatrixLocation = glGetUniformLocation(program, "viewMatrix"); // Get the location of our view matrix in the shader
    int modelMatrixLocation = glGetUniformLocation(program, "modelMatrix"); // Get the location of our model matrix in the shader
    
    
    glUniformMatrix4fv(projectionMatrixLocation, 1, GL_FALSE, &projectionMatrix[0][0]); // Send our projection matrix to the shader
    glUniformMatrix4fv(viewMatrixLocation, 1, GL_FALSE, &viewMatrix[0][0]); // Send our view matrix to the shader
    glUniformMatrix4fv(modelMatrixLocation, 1, GL_FALSE, &modelMatrix[0][0]); // Send our model matrix to the shader
    
    
	 //// The Shader Program ends here
    //// START TO READ AGAIN
    //// START TO READ AGAIN
    //// START TO READ AGAIN
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    


    glBindAttribLocation(program, 0, "in_Position");
    glBindAttribLocation(program, 1, "in_Color");
    

    
    // this creates the scene
    setupScene();
    
    int i=0;

    // Enable depth test
    // ignore this line, it allows us to keep the distance value after we proejct each object to a 2d canvas.
    glEnable(GL_DEPTH_TEST);
    
    // This is our render loop. As long as our window remains open (ESC is not pressed), we'll continue to render things.
    while(!glfwWindowShouldClose(window))
    {
        
        // Clear the entire buffer with our green color (sets the background to be green).
        glClearBufferfv(GL_COLOR , 0, clear_color);
        glClearBufferfv(GL_DEPTH , 0, clear_depth);
        
        // this draws the coordinate system
		coordinate_system_renderer->draw();
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //// This generate the object
        // Enable the shader program
        glUseProgram(program);
        
        // this changes the camera location
        glm::mat4 rotated_view = viewMatrix * GetRotationMatrix();
        glUniformMatrix4fv(viewMatrixLocation, 1, GL_FALSE, &rotated_view[0][0]); // send the view matrix to our shader
        

        // This moves the model to the right
        modelMatrix = glm::translate(glm::mat4(0.5f), glm::vec3(2.0f, 0.0f, 0.0f));
        glUniformMatrix4fv(modelMatrixLocation, 1, GL_FALSE, &modelMatrix[0][0]); // Send our model matrix to the shader
        

		//This line renders your triangle strip model
        renderTriangleStripModel();
        

        // This moves the model to the left
        modelMatrix = glm::translate(glm::mat4(0.5f), glm::vec3(-2.0f, -0.0f, 0.0f));
        glUniformMatrix4fv(modelMatrixLocation, 1, GL_FALSE, &modelMatrix[0][0]); // Send our model matrix to the shader
        
		// This line renders your Ppolygon model
        renderTriangleModel();
        
        
		// disable the shader program
        glUseProgram(0);


        //// This generate the object
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        
        // Swap the buffers so that what we drew will appear on the screen.
        glfwSwapBuffers(window);
        glfwPollEvents();
        
    }
    
	// delete the coordinate system object
	delete coordinate_system_renderer;

    // Program clean up when the window gets closed.
    glDeleteVertexArrays(2, vaoID);
    glDeleteProgram(program);
}

